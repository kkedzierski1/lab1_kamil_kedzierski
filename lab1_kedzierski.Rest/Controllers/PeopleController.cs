﻿using lab1_kedzierski.rest.Context;
using lab1_kedzierski.rest.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lab1_kedzierski.rest.Controllers
{
    [Route("/api/{controller}")]
    public class PeopleController : ControllerBase
    {
        private readonly AzureDbContext dbContext;
        public PeopleController(AzureDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Person>> Get()
        {
            var people = dbContext.People.ToList();
            return people;
        }

        [HttpPost]
        public void Post([FromBody] Person person)
        {
            if (ModelState.IsValid)
            {
                dbContext.Add(person);
                dbContext.SaveChanges();
            }
            
        }

        [HttpPut]
        public void Put(int id, [FromBody] Person person)
        {
            if (ModelState.IsValid)
            {
                var personInDb = dbContext.People.SingleOrDefault(p => p.PersonId == id);
                personInDb.FirstName = person.FirstName;
                personInDb.LastName = person.LastName;
                dbContext.SaveChanges();
            }
        }

        [HttpGet("{id}")]
        public ActionResult<Person> Get(int id)
        {
            var person = dbContext.People.SingleOrDefault(p => p.PersonId == id);            
            return person;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var person = dbContext.People.SingleOrDefault(p => p.PersonId == id);
            dbContext.People.Remove(person);
            dbContext.SaveChanges();
        }
    }
}
